# -*- coding: utf-8 -*-

from odoo import models, fields, api


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    order_line_image = fields.Binary(
        related="product_id.image_1920",
    )
