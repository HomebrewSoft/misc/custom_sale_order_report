{
    "name": "Custom Sale Order Report",
    "version": "13.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "depends": [
        "sale",
        "sale_management",
        "sale_stock",
        "sale_product_matrix",
        "website_sale",
    ],
    "data": [
        "reports/sale_order.xml",
    ],
}
